package Tests;

import Utils.base;
import Verifications.verify;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;
import  org.testng.annotations.Listeners;


@Listeners(listeners.class)
public class cases extends base
{

     @BeforeMethod
     public void beforeMethod()
     {
        todoF.createItems();
     }

    @AfterMethod
    public void afterMethod()
    {
        todoP.clickFilterAll();
        todoF.deleteAllItems();

    }

    // check created three items like array size
    @Test
    public void test01()
    {
        verify.verifyCountElement(todoP.getItemSize(),3);
    }

    // check after tooggle element in completed list only one
    @Test
    public void test02()
    {
        todoF.toggleElement(2);
        todoP.clickFilterCompleted();
        verify.verifyCountElement(todoP.getItemSize(), 1);
    }


    //check after tooggle element in active list two
    @Test
    public void test03()
    {
        System.out.println("Test 03 started");
        todoF.toggleElement(2);
        todoP.clickFilterActive();
        verify.verifyCountElement(todoP.getItemSize(), 2);
        System.out.println("Test 03 ended");
    }


    // check clear completed after toggle element
    @Test
    public void test04()
    {
        System.out.println("Test 04 started");
        todoF.toggleElement(2);
        todoP.clickClearCompleted();
        verify.verifyCountElement(todoP.getItemSize(), 2);
        System.out.println("Test 04 ended");

    }

    // check number items left
    @Test
    public void test05()
    {
        System.out.println("Test 05 started");
        todoF.toggleElement(2);
        verify.verifyCountElement(todoP.getItemLeftNum(), 2);
        System.out.println("Test 05 ended");
    }

    // rename item
    @Test
    public void test06()
    {
        System.out.println("Test 06 started");

        WebElement elList = todoP.getlistItems().get(0);
        String oldText = elList.getText();
        String newText = "ABCDE";
        todoF.renameItem(elList, newText);

        verify.verifyText(elList, newText);

        System.out.println("Test 06 ended");
    }

    //delete item
    @Test
    public void test07()
    {
        System.out.println("Test 07 started");
        todoF.deleteItem(0);
        verify.verifyCountElement(todoP.getItemSize(), 2);
        System.out.println("Test 07 ended");
    }

}
