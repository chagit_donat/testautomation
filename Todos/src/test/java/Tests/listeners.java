package Tests;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class listeners implements ITestListener
{
    public void onFinish(ITestContext arg0)
    {
        System.out.println("finish check: " + arg0.getName()+ " ended!");
    }

    public void onStart(ITestContext arg0)
    {
        System.out.println("begging check: " + arg0.getName() + " started!");
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0)
    {
        // TODO Auto-generated method stub
    }

    public void onTestFailure(ITestResult arg0)
    {
        System.out.println("Test failure " + arg0.getName());
    }

    public void onTestSkipped(ITestResult arg0)
    {
        // TODO Auto-generated method stub
    }

    public void onTestStart(ITestResult arg0)
    {
        System.out.println("Test started " + arg0.getName());
    }

    public void onTestSuccess(ITestResult arg0)
    {
        System.out.println("Test Successed " + arg0.getName());
    }
}
