package Flows;

import Utils.base;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class todoFlow extends base
{
    public void createItems()
    {
        String[] arrStrs = {"AAA", "BBB", "CCC"};
        for (String str: arrStrs)
        {
            todoP.createItem.sendKeys(str);
            todoP.createItem.sendKeys(Keys.ENTER);
        }

    }

    public void deleteItem(int index)
    {
        WebElement current = todoP.getlistItems().get(index);
        deleteItem(current);
    }

    private void deleteItem(WebElement elem)
    {
        WebElement del = elem.findElement(By.tagName("button"));
        action.moveToElement(elem).moveToElement(del).click().build().perform();
    }

    public void deleteAllItems()
    {
        for (WebElement el: todoP.getlistItems())
        {
            deleteItem(el);
            //System.out.println("delete element : " + el);
        }

        //System.out.println("deleted all elements");
    }

    public void toggleElement(int index)
    {
        todoP.getlistItems().get(index).findElement(By.className("toggle")).click();
    }

    public void renameItem(WebElement elem, String newName)
    {
        String text = elem.getText();
        action.doubleClick(elem).build().perform();
        for (int i = 0; i < text.length(); i++)
        {
            elem.findElement(By.className("edit")).sendKeys(Keys.BACK_SPACE);
            // by tag name not working because there are nany input tags under this li
        }
        elem.findElement(By.className("edit")).sendKeys(newName);
        elem.findElement(By.className("edit")).sendKeys(Keys.RETURN);
    }

}
