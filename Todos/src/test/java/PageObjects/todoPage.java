package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;

import java.util.List;

public class todoPage
{
    @FindBy(how = How.CLASS_NAME, using = "new-todo")
    public WebElement createItem;

    @FindBy(how = How.XPATH, using = "//ul[@class='todo-list']/li")
    private List<WebElement> listItems;

    @FindBy(how = How.LINK_TEXT, using = "All")
    private WebElement filterAll;

    @FindBy(how = How.LINK_TEXT, using = "Completed")
    private WebElement filterCompleted;

    @FindBy(how = How.LINK_TEXT, using = "Active")
    private WebElement filterActive;

    @FindBy(how = How.CLASS_NAME, using = "clear-completed")
    private WebElement clearCompletedBtn;

    @FindBy(how = How.XPATH, using = "//span[@class='todo-count']/strong")
    private WebElement numItemLeft;


    public void clickFilterAll()
    {
            filterAll.click();
    }

    public void clickFilterCompleted()
    {
        filterCompleted.click();
    }

    public void clickFilterActive()
    {
        filterActive.click();
    }

    public void clickClearCompleted()
    {
        clearCompletedBtn.click();
    }

    public List<WebElement> getlistItems()
    {
        return listItems;
    }

    public int getItemSize()
    {
        return getlistItems().size();
    }

    public int getItemLeftNum()
    {
        return Integer.parseInt(numItemLeft.getText());
    }




}
