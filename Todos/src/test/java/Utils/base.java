package Utils;

import Flows.todoFlow;
import PageObjects.todoPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class base
{
    protected static WebDriver driver;
    protected static Actions action;
    protected static todoPage todoP;
    protected static todoFlow todoF;

    @BeforeClass
    public static void startSession()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("http://todomvc.com/examples/react");
        action = new Actions(driver);
        todoP = PageFactory.initElements(driver, todoPage.class);
        todoF = PageFactory.initElements(driver, todoFlow.class);
    }


    @AfterClass
    public static void closeSession() throws InterruptedException
    {
        //Thread.sleep(2000);
        driver.quit();
    }

}
