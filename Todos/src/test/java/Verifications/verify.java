package Verifications;

import Utils.base;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class verify extends base
{
    public static void verifyCountElement(int actual, int expected)
    {
        Assert.assertEquals(actual, expected);
    }

    public static void verifyText(WebElement elem, String expected)
    {
        Assert.assertEquals(elem.getText(), expected);
    }
}
