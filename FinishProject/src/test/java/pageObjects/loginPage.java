package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class loginPage
{
    @FindBy(how = How.ID, using = "")
    public WebElement userName;

    @FindBy(how = How.ID, using = "")
    public WebElement password;

    @FindBy(how = How.ID, using = "")
    public WebElement login;

    @FindBy(how = How.CSS, using = "span[ng-if='response.error']")
    public WebElement errorMessage;


}
