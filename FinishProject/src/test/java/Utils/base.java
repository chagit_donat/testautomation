package Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.w3c.dom.Document;
import pageObjects.headerPage;
import pageObjects.loginPage;

import javax.security.sasl.SaslException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class base

{
    protected static WebDriver driver;

    protected static headerPage hP;
    protected static loginPage lP;

    public static String getDataXml(String tagName) throws ParserConfigurationException, SaslException, IOException
    {
        DocumentBuilder dBuilder;
        Document doc = null;
        //take the path from the root project
        File fXmlFile = new File("./dataConfiguration.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try
        {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
        } catch (Exception e)
        {
            System.out.println("Exception in reading XML file: " + e);
        }
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName(tagName).item(0).getTextContent();
    }

    @BeforeClass
    public static void startSession() throws ParserConfigurationException, IOException, SaslException
    {
        System.setProperty("webdriver.chrome.driver", getDataXml("ChromeDriverPath"));
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(getDataXml("URL"));
        hP = PageFactory.initElements(driver, headerPage.class);
        lP = PageFactory.initElements(driver, loginPage.class);
    }

    @AfterClass
    public static void closeSession()
    {
        driver.quit();
    }



}
