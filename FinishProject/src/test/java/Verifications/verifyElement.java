package Verifications;

import Utils.base;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import static org.junit.Assert.fail;

public class verifyElement extends base
{
    public static void verifyText(WebElement element, String expected)
    {
        try
        {
            // junit Assert.assertEquals(expected, element.getText());
            Assert.assertEquals(element.getText(), expected);

            System.out.println(expected + "is displayed");

        }
        catch (AssertionError e)
        {
            System.out.println(expected + "is not display, see trace: " + e);
            fail(expected + "is not display, see trace: " + e);
        }

    }
}
