package Lesson01_TestNG;

import org.testng.annotations.*;


public class TestNGExample
{
    @BeforeClass
    public void beforeClass()
    {
        System.out.println("before classs");
    }

    @BeforeMethod
    public void beforeMethod()
    {
        System.out.println("before method");
    }

    @Test(dependsOnMethods = {"test01"})
    public void test02()
    {
        System.out.println("test02");
    }
    @Test
    public void test01()
    {
        System.out.println("test01");
    }

    @AfterMethod
    public void  afterMethod()
    {
        System.out.println("after method");
    }

    @AfterClass
    public void afterClass()
    {
        System.out.println("after class");
    }
    //nir

}
