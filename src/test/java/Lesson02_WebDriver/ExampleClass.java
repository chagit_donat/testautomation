package Lesson02_WebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ExampleClass
{

    WebDriver driver;
    String title = "imdb",url= "a.com";
    String actualTitle, actualUrl;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("http://imdb.com");
        driver.manage().window().maximize();
    }

    @Test
    public void test01() throws InterruptedException
    {
        actualTitle = driver.getTitle();
        actualUrl = driver.getCurrentUrl();

        driver.navigate().refresh();


        System.out.println("Exepted title is " + title +  ", Actual title is " + actualTitle);
        System.out.println("Exepted url is " + url + ", Actual url is " + actualUrl);

        if(driver.getTitle().equals(title))
        {
            System.out.println("title equal!!");
        }
        else
        {
            System.out.println("title not equal!");
        }

        if(driver.getCurrentUrl().equals(url)){
            System.out.println("url equal!!");
        }else{
            System.out.println("url not equal!!");
        }
        //Thread.sleep(3000);
    }

    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }

}
