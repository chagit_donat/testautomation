package GraphicElements;

import PageObjectOrange.DashbordPage;
import PageObjectOrange.LoginPage;
import PageObjectOrange.MainPage;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class example01
{
    WebDriver driver;


    @BeforeClass
    public void startSession()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https:google.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void test01()
    {
        takeElementImage(driver.findElement(By.id("hplogo")));
        verifyElementImage(driver.findElement(By.id("hplogo")));

    }

    @AfterClass
    public void closeSession()
    {
        driver.quit();
    }

    public void takeElementImage(WebElement imageElement)
    {
        try
        {
            Screenshot imageScreenShot = new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, imageElement);
            ImageIO.write(imageScreenShot.getImage(),"png", new File("./Images/logo.png"));
        }
        catch (Exception e)
        {
            System.out.println("Error writing image to file");
        }


    }

    public void verifyElementImage(WebElement imageElement)
    {
        BufferedImage expectedImage = null;
        try
        {
            //expectedImage = ImageIO.read(new File("./Images/mvn-logo.png"));
            expectedImage = ImageIO.read(new File("./Images/logo.png"));
        }
        catch (Exception e)
        {
            System.out.println("Error readin image, details:" + e);
        }

        Screenshot imageScreenShot = new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, imageElement);
        BufferedImage actualImage = imageScreenShot.getImage();
        ImageDiffer imgDiff = new ImageDiffer();
        ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);

        //
        Assert.assertFalse(diff.hasDiff());
    }

}
