package synchronization;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.DriverManager;

public class example02
{
    WebDriver driver;
    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        //driver.get("C:\\Users\chagit.donat\Desktop\syncExam\a.html");
    }

    @Test
    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }

}
