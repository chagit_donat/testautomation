package synchronization;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class example01
{
    WebDriver driver;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/ex_synchronization.html");
    }


    @Test
    public void test01()
    {
        driver.findElement(By.xpath("//div[@id='start2']/button")).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='finish2']/h4")));

        String text= driver.findElement(By.id("finish2")).getText();

        Assert.assertEquals(text,"My Rendered Element After Fact!");
    }

    @Test
    public void test02() throws InterruptedException
    {
        driver.findElement(By.xpath("//div[@id='start1']/button")).click();
        Thread.sleep(10000);
        System.out.println(driver.findElement(By.cssSelector("div[id='loading1'][style='display: none;']")));
    }

    @Test
    public void test03()
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//div[@id='contact_info_left']/form/button")).click();
        System.out.println(driver.findElement(By.id("message")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.id("message")).isDisplayed());
    }

    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }

}
