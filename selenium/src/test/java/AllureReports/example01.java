package AllureReports;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.w3c.dom.Document;

import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class example01
{
    WebDriver driver;

    @BeforeClass
    public void openSession()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(getDataXml("url"));

    }

    @Test(priority = 0,description = "Check BMI")
    @Description("Insert Weight & Height")
    public void test01()
    {
        updateWeightField();
        //WebElement weight = driver.findElement(By.id("weight"));
        WebElement hight = driver.findElement(By.id("hight"));

        //weight.sendKeys(getDataXml("weight"));
        hight.sendKeys(getDataXml("height"));

        /*String w = weight.getText();
        String h = hight.getText();

        System.out.println("weight is " + w + ", hight is " + h);*/
        WebElement calc = driver.findElement(By.id("calculate_data"));
        calc.click();
        try
        {
            resultBMI();
        }
        catch (InterruptedException e)
        {
            System.out.println("ResultBMI failed, the error " + e);
        }

    }

    @Step("Update Wait Field")
    public void updateWeightField()
    {
        driver.findElement(By.id("weight")).sendKeys(getDataXml("weight"));
    }



    //@Step("Verify Expected Result BMI")
    public void resultBMI() throws InterruptedException
    {
        Thread.sleep(5000);
        String expectedResult = getDataXml("exeptedResult");
        String actualResult = driver.findElement(By.id("bmi_result")).getAttribute("value");
        try
        {
            Assert.assertEquals(actualResult,expectedResult);
        }
        catch (AssertionError e)
        {
            System.out.println("Error On Assert, " + e);
        }

    }

    @AfterClass
    public void closSession()
    {
        driver.quit();
    }

    public String getDataXml(String tagName)
    {
        DocumentBuilder dBuilder;
        Document doc = null;
        //take the path from the root project
        File fXmlFile = new File("./config.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try
        {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
        } catch (Exception e)
        {
            System.out.println("Exception in reading XML file: " + e);
        }
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName(tagName).item(0).getTextContent();
    }
}
