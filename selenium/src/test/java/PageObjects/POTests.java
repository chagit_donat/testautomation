package PageObjects;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class POTests
{
    WebDriver driver;
    LoginPage login;
    FormPage form;
    ClickPage click;

    @BeforeClass
    public void openSession()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/webdriveradvance.html");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        login = PageFactory.initElements(driver, LoginPage.class);
        form = PageFactory.initElements(driver, FormPage.class);
        click = PageFactory.initElements(driver, ClickPage.class);
    }

    @Test(priority = 1)
    public void testLogin()
    {

        login.action("selenium", "webdriver");



    }

    @Test(priority = 2)
    public void testForm()
    {
        form.action("aaa", "bbb", "ccc");
    }

    @Test(priority = 3)
    public void testClick()
    {
        Assert.assertTrue(click.getBtn_clickMe().isDisplayed());
    }


    @AfterClass
    public void closeSession()
    {
        driver.quit();
    }
}
