package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage
{
    @FindBy(how = How.NAME, using = "username2")
    private WebElement txt_userName;

    @FindBy(how = How.NAME, using = "password2")
    private WebElement txt_password;

    @FindBy(how = How.ID, using = "submit")
    private WebElement btn_submit;

    public void action(String userName, String password)
    {
        txt_userName.sendKeys(userName);
        txt_password.sendKeys(password);
        btn_submit.click();
    }

}
