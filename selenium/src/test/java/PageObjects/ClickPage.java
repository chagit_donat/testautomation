package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ClickPage
{
    @FindBy(how = How.XPATH, using = "//a/button")
    private WebElement btn_clickMe;


    public WebElement getBtn_clickMe()
    {
        return btn_clickMe;
    }

}
