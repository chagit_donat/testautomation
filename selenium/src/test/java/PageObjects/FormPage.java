package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class FormPage
{
    @FindBy(how = How.ID, using = "occupation")
    private WebElement txt_occupation;

    @FindBy(how = How.ID, using = "age")
    private WebElement txt_age;

    @FindBy(how = How.ID, using = "location")
    private WebElement txt_location;

    @FindBy(how = How.XPATH, using = "//a/input[@value='Click Me']")
    private WebElement btn_clickDetails;

    public void action(String occ, String age, String loc)
    {
        txt_occupation.sendKeys(occ);
        txt_age.sendKeys(age);
        txt_location.sendKeys(loc);
        btn_clickDetails.click();
    }
}
