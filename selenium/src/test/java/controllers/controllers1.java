package controllers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.Array;

public class controllers1
{
    WebDriver driver;
    String firstName = "Chagit";
    String lastName = "Doant";

    @BeforeClass
    public void beforeSession()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/ex_controllers.html");
    }

    @Test
    public void test01()
    {
        WebElement firstNameElement = driver.findElement(By.name("firstname"));
        firstNameElement.sendKeys(firstName);
        System.out.println("first name element is " + firstNameElement);

        WebElement lastNameElement = driver.findElement(By.name("lastname"));
        lastNameElement.sendKeys(lastName);
        System.out.println("last name element is " + lastNameElement.getText());

        Select countrySelect = new Select(driver.findElement(By.id("continents")));
        countrySelect.selectByValue("africa"); // countrySelect.selectByVisibleText("Africa");

        System.out.println("country element is " + countrySelect.getAllSelectedOptions());

        System.out.println("Test 01 Passed Successfully!!!");
    }

    @Test(enabled = true)
    public void test02() throws InterruptedException
    {

        driver.navigate().refresh();
        driver.findElement(By.xpath("//*/input[@value='Male']")).click();
        driver.findElement(By.cssSelector("input[value='7']")).click();
        //driver.findElement(By.name("datepicker")).sendKeys("01/19/2021");

        driver.findElement(By.id("datepicker")).click();
        WebElement dateWidget = driver.findElement(By.id("ui-datepicker-div"));
        for (WebElement cell: dateWidget.findElements(By.tagName("td")))
        {
            if (cell.getText().equals("19"))
            {
                cell.click();
                break;
            }
        }

        Thread.sleep(2000);

        driver.findElement(By.xpath("//input[@value='Automation_Tester']")).click();
        driver.findElement(By.id("tool-2")).click();
        Select seleniumSelect = new Select(driver.findElement(By.name("selenium_commands")));
        seleniumSelect.selectByIndex(1);
        driver.findElement(By.id("photo")).sendKeys("C:\\Users\\chagit.donat\\Pictures\\Saved Pictures\\Untitled.png");

        System.out.println("Test 02 Passed Successfully!!!");
    }

    @Test(enabled = true)
    public void test03()
    {

        driver.findElement(By.id("submit")).click();

        String currentUrlStr = driver.getCurrentUrl();
        if(currentUrlStr.contains("Chagit") && currentUrlStr.contains("Donat"))
        {
            System.out.println("Test Passed");
        }
        else
        {
            System.out.println("Test Failed");
        }

        //currentUrlStr = "https://atidcollege.co.il/Xamples/ex_controllers.html?firstname=&lastname=&sex=Male&exp=1&datepicker=01%2F19%2F2021&profession=Automation_Tester&photo=&tool=Selenium_Webdriver&continents=africa&selenium_commands=wait&submit=";


        System.out.println("current url str " + currentUrlStr);

        String dateFormat = currentUrlStr.split("&")[4];
        System.out.println("dateFormat " + dateFormat);

        String[] arr = dateFormat.split("%2F");

        System.out.println(arr[2] + "/" + arr[1] + "/" + arr[0].substring(arr[0].length() - 2)  );


        System.out.println("Test 03 Passed Successfully!!!");
    }

    @AfterClass
    public void closeSession()
    {
        driver.quit();
    }
}
