package ErrorHandling;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;

@Listeners(listeners.class)
public class example02
{
    WebDriver driver;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/ex_synchronization.html");
    }

    @Test
    public void test01() throws InterruptedException
    {
        driver.findElement(By.xpath("//div[@id='contact_info_left']/form/button")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("checkbox"));
    }

    @AfterClass
    public void afterClass()
    {
        driver.close();
    }

}
