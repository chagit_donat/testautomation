package ErrorHandling;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;

public class example01
{
    WebDriver driver;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/ex_synchronization.html");
    }

    @Test
    public void test01() throws InterruptedException
    {


        //try
        //{
            driver.findElement(By.xpath("//div[@id='contact_info_left']/form/button")).click();
            Thread.sleep(5000);
        //    driver.findElement(By.id("checkbox"));
        //    System.out.println("checkbox exists!");
        //}
        //catch (Exception e)
        //{
            System.out.println("The checkbox element not exists in dom");
        //}

        if(!driver.findElements(By.id("checkbox")).isEmpty())
        {
            System.out.println("element exists");
        }else
        {
            System.out.println("element not exists");
        }




    }

    @AfterClass
    public void afterClass()
    {
        driver.close();
    }

}
