package ActionsAndTricks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

//@Listeners(listeners1.class)
public class example02
{
    WebDriver driver;
    Actions act;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://marcojakob.github.io/dart-dnd/basic/");
        act = new Actions(driver);
    }

    @Test
    public void test01()
    {

        WebElement source = driver.findElement(By.className("document"));
        WebElement target = driver.findElement(By.className("trash"));
        act.dragAndDrop(source, target).build().perform();

        Assert.assertTrue(driver.findElement(By.cssSelector("div[class='trash full']")).isDisplayed());
    }

    @AfterClass
    public void afterClass() throws InterruptedException
    {
        Thread.sleep(5000);
        driver.quit();
    }

}
