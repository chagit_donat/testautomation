package ActionsAndTricks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class example01
{
    WebDriver driver;
    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/ex_actions.html");
    }


    @Test
    public void test01()
    {

       WebElement draggable = driver.findElement(By.id("draggable"));
       WebElement droppable = driver.findElement(By.id("droppable"));
        Actions dgDr = new Actions(driver);
        dgDr.dragAndDrop(draggable, droppable).build().perform();

        try
        {
            Assert.assertEquals(driver.findElement(By.xpath("//div[@id='droppable']/p")).getText(), "Dropped!");
        }
        catch (AssertionError e)
        {
            System.out.println("Assert error, not like expected value, " + e);
        }

    }

    @Test
    public void test02() throws InterruptedException
    {
        List<WebElement> selectList = driver.findElements(By.cssSelector("[class='ui-widget-content ui-selectee']"));
        Actions act = new Actions(driver);
        act.clickAndHold(selectList.get(1)).clickAndHold(selectList.get(2)).click().build().perform();
        Thread.sleep(2000);
    }

    @Test
    public void test03()
    {
        WebElement dbClick = driver.findElement(By.id("dbl_click"));
        Actions act = new Actions(driver);
        act.doubleClick(dbClick).build().perform();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Assert.assertTrue(driver.findElement(By.id("demo")).isDisplayed());
    }

    @Test
    public void test04()
    {
        WebElement noHo = driver.findElement(By.id("mouse_hover"));
        Actions act = new Actions(driver);
        act.moveToElement(noHo).build().perform();
        Assert.assertEquals(noHo.getAttribute("style"), "background-color: rgb(255, 255, 0);");
    }

    @Test
    public void test05()
    {

        //option 1
        //JavascriptExecutor js = ((JavascriptExecutor) driver);
        //js.executeScript("");

        //option 2
        ((JavascriptExecutor) driver).executeScript("scrollTo(0,1000)");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Assert.assertTrue(driver.findElement(By.id("scrolled_element")).isDisplayed());
    }


    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }
}
