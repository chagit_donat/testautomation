package switchNavigation;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class example01
{
    WebDriver driver;

    @BeforeClass
    public void beforeClass(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/ex_switch_navigation.html");
    }

    @Test
    public void test01()
    {
        driver.findElement(By.id("btnAlert")).click();
        Alert al = driver.switchTo().alert();
        System.out.println(al.getText());
        al.accept();
        Assert.assertEquals(driver.findElement(By.id("output")).getText(), "Alert is gone.");
    }

    @Test
    public void test02()
    {
        driver.findElement(By.id("btnPrompt")).click();
        Alert pl = driver.switchTo().alert();
        System.out.println(pl.getText());
        pl.sendKeys("Chagit");
        pl.accept();
        Assert.assertEquals(driver.findElement(By.id("output")).getText(), "Chagit");
    }

    @Test
    public void test03()
    {
        WebElement frm = driver.findElement(By.cssSelector("[src='ex_switch_newFrame.html']"));
        //System.out.println(frm);
        driver.switchTo().frame(frm);

        String text = driver.findElement(By.id("iframe_container")).getText();
        System.out.println(text);
        driver.switchTo().defaultContent();

        Assert.assertEquals(text, "This is an IFrame !");
    }

    @Test
    public void test04()
    {
        driver.findElement(By.id("btnNewTab")).click();
        Set<String> win = driver.getWindowHandles();
        String exeptedValue = "This is a new tab";

        for (String s : win)
        {
            System.out.println(s);
            driver.switchTo().window(s);
            if(driver.getCurrentUrl().equals("https://atidcollege.co.il/Xamples/ex_switch_newTab.html"))
                break;
        }
        String text = driver.findElement(By.id("new_tab_container")).getText();
        System.out.println(text);

        Assert.assertEquals(text,exeptedValue);
        // return to main window

        driver.close();
        List<String> winl = new ArrayList<>(win);
        driver.switchTo().window(winl.get(0));
    }

    @Test
    public void test05()
    {
        driver.findElement(By.linkText("Open New Window")).click();
        ArrayList<String> winList = new ArrayList<>(driver.getWindowHandles());
        String exeptedString = "This is a new window";

        //move to new tab
        driver.switchTo().window(winList.get(1));
        String text = driver.findElement(By.id("new_window_container")).getText();
        System.out.println(text);
        System.out.println("The title is " + driver.getTitle());

        Assert.assertEquals(text,exeptedString);
        driver.close();

        //return to first window
        driver.switchTo().window(winList.get(0));
        System.out.println("The title is " + driver.getTitle());



    }

    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }

}
