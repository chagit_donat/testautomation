package Lcators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class LocatorsBasic
{
    WebDriver driver;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://www.selenium.dev/");
    }

    @Test
    public void test01()
    {

        WebElement elem = driver.findElement(By.className("nav-item"));
        WebElement elem1 = driver.findElement(By.linkText("Blog"));
        System.out.println(elem);
        System.out.println(elem1);

        //List<WebElement> links1 = driver.findElements(By.linkText(""));

        System.out.println("Count of links page is " + driver.findElements(By.linkText("")).size());


        System.out.println("count of Selenium " + driver.findElements(By.partialLinkText("Selenium")).size());
        System.out.println("count of Selenium " + driver.findElements(By.partialLinkText("selenium")).size());

    }

    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }
}
