package Lcators;

import com.google.common.collect.Lists;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class LoacatorBasic01
{
    WebDriver driver;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://www.hamichlol.org.il/");
    }

    @Test
    public void test()
    {

        List<WebElement> marksElements =  driver.findElements(By.className("nomobile"));

        for (WebElement elem : marksElements){
            System.out.println("element is " + elem);
        }

        for (WebElement elem : Lists.reverse(marksElements)) {
            System.out.println("element reverse is " + elem);
        }


    }

    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }
}
