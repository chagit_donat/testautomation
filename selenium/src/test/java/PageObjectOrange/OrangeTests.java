package PageObjectOrange;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class OrangeTests
{
    WebDriver driver;
    LoginPage login;
    MainPage menu;
    DashbordPage dash;

    @BeforeClass
    public void startSession()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        login = PageFactory.initElements(driver, LoginPage.class);
        menu = PageFactory.initElements(driver, MainPage.class);
        dash = PageFactory.initElements(driver, DashbordPage.class);
        login.loginAction("Admin", "admin123");

    }

    @Test
    public void test01()
    {

        System.out.println("test01");
        Assert.assertEquals(menu.getFieldText(), "Welcome Admin");


    }

    @Test
    public void test02()
    {
        System.out.println("test02");
        Assert.assertEquals(dash.getFieldText(), "Dashbord");
    }



    @AfterClass
    public void closeSession()
    {
        driver.quit();
    }
}
