package PageObjectOrange;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DashbordPage
{
    @FindBy(how = How.XPATH, using = "//div[@class='head']/h1")
    private WebElement field_dashbord;

    public String getFieldText()
    {
        return field_dashbord.getText();
    }
}
