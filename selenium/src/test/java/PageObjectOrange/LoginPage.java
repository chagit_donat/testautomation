package PageObjectOrange;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage
{
    @FindBy(how = How.ID, using = "txtUsername")
    private WebElement txt_userName;

    @FindBy(how = How.ID, using = "txtPassword")
    private WebElement txt_password;

    @FindBy(how = How.ID, using = "btnLogin")
    private WebElement btn_submit;

    public void loginAction(String user, String pass)
    {
        txt_userName.sendKeys(user);
        txt_password.sendKeys(pass);
        btn_submit.click();
    }
}
