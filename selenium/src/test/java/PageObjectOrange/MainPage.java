package PageObjectOrange;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MainPage
{
    @FindBy(how = How.ID, using = "welcome")
    private WebElement field_welcome;

    public String getFieldText()
    {
        return  field_welcome.getText();
    }

}
