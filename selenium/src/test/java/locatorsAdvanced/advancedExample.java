package locatorsAdvanced;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class advancedExample
{
    WebDriver driver;

    @BeforeClass
    public void beforeSession()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/ex_locators.html");
    }

    @Test
    public void testSession()
    {
        System.out.println(driver.findElement(By.id("locator_id")));
        System.out.println(driver.findElement(By.name("locator_name")));
        System.out.println(driver.findElement(By.xpath("//div/div/div/p")));
        System.out.println(driver.findElement(By.className("locator_class")));

        //System.out.println(driver.findElement(By.cssSelector("[class='locator_class']")));

        System.out.println(driver.findElement(By.linkText("myLocator(5)")));
        System.out.println(driver.findElement(By.partialLinkText("my locator (6)")));
        System.out.println(driver.findElement(By.cssSelector("[myname='selenium']")));
        System.out.println(driver.findElement(By.tagName("button")));

    }

    @AfterClass
    public void afterSession()
    {
        driver.quit();
    }
}
