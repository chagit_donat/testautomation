package assertVerification;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.*;

import java.sql.Driver;
import java.sql.DriverManager;

public class example
{
    WebDriver driver;

    @BeforeClass
    public void beforeSession(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://atidcollege.co.il/Xamples/bmi/");

    }

    @Test
    public void test01() throws InterruptedException
    {

        WebElement weight = driver.findElement(By.id("weight"));
        WebElement hight = driver.findElement(By.id("hight"));

        weight.sendKeys("60");
        hight.sendKeys("150");

        /*String w = weight.getText();
        String h = hight.getText();

        System.out.println("weight is " + w + ", hight is " + h);*/

        WebElement calc = driver.findElement(By.id("calculate_data"));
        calc.click();

        Thread.sleep(5000);



        String expectedResult = "27";
        String actualResult = driver.findElement(By.id("bmi_result")).getAttribute("value");

        Assert.assertEquals(actualResult,expectedResult);

    }

    @Test
    public void test02()
    {
        WebElement btnCalc = driver.findElement(By.id("calculate_data"));
        System.out.println(btnCalc.getSize());
        System.out.println(btnCalc.getLocation().getX());
        System.out.println(btnCalc.getLocation().getY());

        System.out.println(btnCalc.isEnabled());
        Assert.assertTrue(btnCalc.isEnabled());
        System.out.println(btnCalc.isDisplayed());
        Assert.assertTrue(btnCalc.isDisplayed());
        System.out.println(btnCalc.isSelected());
        Assert.assertFalse(btnCalc.isSelected());

        System.out.println(btnCalc.getTagName());
        Assert.assertEquals(btnCalc.getTagName(),"input");
        System.out.println(btnCalc.getAttribute("value"));
        Assert.assertEquals(btnCalc.getAttribute("value"), "Calculate BMI");

        System.out.println(driver.findElement(By.id("new_input")).isDisplayed());
        Assert.assertFalse(driver.findElement(By.id("new_input")).isDisplayed());



    }

    @AfterClass
    public void afterClass(){
        driver.quit();
    }
}
