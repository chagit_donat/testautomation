package assertVerification;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class example01
{

    WebDriver driver;

    @BeforeClass
    public void beforeClass()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.nopcommerce.com/camera-photo");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


    }

    @Test
    public void test01()
    {
        WebElement sortElement = driver.findElement(By.id("products-orderby"));
        Select sort = new Select(sortElement);
        sort.selectByVisibleText("Price: Low to High");


    }

    @Test
    public void test02()
    {
        String[] ex = {"Nikon D5500 DSLR", "Apple iCam", "Leica T Mirrorless Digital Camera"};
        List<WebElement> products = driver.findElements(By.className("product-title"));

        for (int i = 0; i < 3; i++)
        {

            Assert.assertEquals(products.get(i).findElement(By.tagName("a")).getText(), ex[i]);
            System.out.println(products.get(i).findElement(By.tagName("a")).getText());
        }
    }

    @Test
    public void test03()
    {
        List<WebElement> stars = driver.findElements(By.xpath("//div[@class='rating']/div"));
        for (WebElement i : stars)
        {
            String width = i.getAttribute("style");

            System.out.println(i.getAttribute("style"));
            String value = width.split(":")[1].substring(1, width.split(":")[1].length() - 2); //
            System.out.println(value);
            Assert.assertTrue(Integer.parseInt(value) >= 0, "verifacation failed, not bigger 0");
        }
    }

    @AfterClass
    public void afterClass()
    {
        driver.quit();
    }
}
