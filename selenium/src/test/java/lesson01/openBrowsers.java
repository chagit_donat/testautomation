package lesson01;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

public class openBrowsers
{
    @Test
    public void test01_chrome()
    {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://chrome.com");
        System.out.println(driver.getTitle());
        driver.quit();

    }

    @Test
    public void test02_firefox()
    {
        WebDriverManager.firefoxdriver().setup();
        WebDriver driver = new FirefoxDriver();
        driver.get("http://firefox.com");
        System.out.println(driver.getTitle());
        driver.quit();

    }

    @Test
    public void test03_internetexplorer()
    {
        WebDriverManager.iedriver().setup();
        WebDriver driver = new InternetExplorerDriver();
        driver.get("https://www.w3schools.com/");
        System.out.println(driver.getTitle());
        driver.quit();

    }
}
